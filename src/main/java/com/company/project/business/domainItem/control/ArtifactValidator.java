package com.company.project.business.domainItem.control;

/**
 *  Control represents an optional class to use with
 *  the domainItem to solve business issues that doesn't
 *  fit into entity or boundary package (ex. Validators,
 *  ...)
 */
public class ArtifactValidator {

    public boolean isValid(String message){
        return true;
    }
}
