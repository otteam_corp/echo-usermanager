package com.company.project.business.domainItem.boundary;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
    Boundary represents the entry point of the application . The frontier
    with outside world . The point that acts as an interface to communicate
    with external elements . Here is where the party begins .

    sample of url ... http://localhost:8080/test-artifact/resources/artifacts
*/

@Path("artifacts")
public class ArtifactResource {

    @Inject
    ArtifactService artifactService;

    @GET
    public String boundaryMethod() {
        //artifactService.getAllArtifacts();
        return "Ok, deployment done and working";
    }

}
