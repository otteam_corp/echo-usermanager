package com.company.project.business.domainItem.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * Entity represent the part of the domain that interacts with the database
 */
@Entity
public class Artifact {

    @Id
    @GeneratedValue
    private long id;
}
